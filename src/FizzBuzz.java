/**
 * Created by jake on 6/23/14.
 */
public class FizzBuzz {

	public static void main(String[] args){

		String fizz = "fizz";
		String buzz = "buzz";

		for (int i = 1; i < 101; i++){

			if (i % 3 == 0){
				System.out.print(fizz);
			}
			if (i % 5 == 0){
				System.out.print(buzz);
			}
			if (i % 5 != 0 && i % 3 != 0){
				System.out.print(i);
			}

			System.out.println();

		}
	}
}

